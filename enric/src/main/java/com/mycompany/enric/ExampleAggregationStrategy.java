package com.mycompany.enric;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class ExampleAggregationStrategy implements AggregationStrategy {

    public Exchange aggregate(Exchange original, Exchange resource) {
        Object originalBody = original.getIn().getBody();
        Object resourceResponse = resource.getOut().getBody();
        Object mergeResult=add(originalBody,resourceResponse);
        		//System.out.println(mergeResult);
        if (original.getPattern().isOutCapable()) {
            original.getOut().setBody(mergeResult);
        } else {
            original.getIn().setBody(mergeResult);
        }
        return original;
    }

	private Object add(Object originalBody, Object resourceResponse) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
    
}
